<?php
/*
Template Name: Blog
*/
?>

<?php get_header() ?>

    <body class="blog">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>Blog</h1>
                        <span>Hear directly from the Amplo experts, partner institutions, and take advantage of data trends that you can leverage today.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="post-listing">
        <div class="container">
            <div class="row search">
                <div class="col-md-12">
                    <form action="#!">
                        <div class="box-form">
                            <input type="search" name="search" placeholder="You looking for...">
                            <button type="submit"></button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row category">
                <div class="col-md-12">
                    <div class="item">
                        <h5><a href="#!">Client Insights</a></h5>
                    </div>
                    <div class="item">
                        <h5><a href="#!">Best Practices</a></h5>
                    </div>
                    <div class="item">
                        <h5><a href="#!">Case Study</a></h5>
                    </div>
                </div>
            </div>
            <div class="row list">
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>Engagement</span></a>
                            </div>
                            <h3><a href="#!">Our 3:1 Email Marketing Rule</a></h3>
                            <p class="summary">
                                Increasing your email open and click-through rates isn’t something that can be changed overnight. Consider the perspective of the audience you are engaging. Do young
                                alumni share the same interests as parents? In this article, we’ll describe some of the basic techniques you can use to increase the number of email opens and clicks
                                with everyone in your constituent base.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-3.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>Engagement</span></a>
                            </div>
                            <h3><a href="#!">Our 3:1 Email Marketing Rule</a></h3>
                            <p class="summary">
                                An excerpt from our Giving Day Handbook: <br><br>
                                Giving Day Team Structure <br><br>
                                Once the Giving Day goals have been put in place, create your overall strategy by building a team to accomplish these goals. In working with leadership, make sure they
                                are aware of the key milestones you are looking to accomplish. As you grow, your team may have varying opinions. Scheduling one to two brainstorming sessions within the
                                planning period will ensure everyone is on the same page.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-1.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>GIVING DAY</span></a>
                            </div>
                            <h3><a href="#!">NOTRE DAME DAY 2017</a></h3>
                            <p class="summary">
                                Notre Dame Day 2017 kicked off on Sunday, April 23rd at 6:42 PM EST. Unlike other Giving Days, Notre Dame Day is a global celebration for the entire Notre Dame family
                                to take action and participate.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>athletics</span></a>
                            </div>
                            <h3><a href="#!">An All Amplo NCAA Basketball Championship?</a></h3>
                            <p class="summary">
                                The Final Four is now set! With March Madness delivering, as always, plenty of upsets and madness to go around. With only three games to go we wanted to highlight how
                                Amplo clients have done in the tournament.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>GIVING DAY</span></a>
                            </div>
                            <h3><a href="#!">Email Schedule When Promoting Giving Day</a></h3>
                            <p class="summary">
                                As many of you have heard, I like to describe Giving Day as the biggest, baddest – on campus- rock show or party of the year! Years ago when touring with an independent
                                band called Salt, we could always tell which promoter properly promoted our concert. We measured it based on how many people were in the seats.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-2.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>CLIENT BEST PRATICES</span></a>
                                <a href="#!"><span>TECHNICAL FAQ</span></a>
                            </div>
                            <h3><a href="#!">AMPLO CLIENT INSIGHTS: INTERCOM</a></h3>
                            <p class="summary">
                                Amplo is proud to launch a brand-new feature, Intercom, that will streamline client support interaction. Intercom will be the center for communication, tracking, and
                                receiving information for all your platform needs. Whether it’s a question on managing a campaign, on overall best practices, on something that isn’t quite working for
                                you, or that you have a new idea for how we can improve the platform, Intercom will be the tool to assist you.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-4.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>giving day</span></a>
                            </div>
                            <h3><a href="#!">GIVING DAY SPOTLIGHT: SEATTLE UNIVERSITY</a></h3>
                            <p class="summary">
                                Amplo, located in Seattle, WA, is proud to partner and launch our first local giving day with Seattle U! The campaign, Seattle U Gives, is the very first giving day
                                being run by SU.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row load-more">
                <div class="col-md-12">
                    <a href="#!"><i class="icon-load"></i></a>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-bottom partner careers blog">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>SCHEDULE A FREE DEMO</h2>
                    <p class="info">
                        Want to the learn more about the Amplo platforms? Have some specific questions specific to your institution or department? Schedule a live demo to get all your burning
                        questions answered!
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SCHEDULE A DEMO</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile odd blog">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption up">MEET THE CONTRIBUTORS THAT BRING YOU</p>
                    <h2 class="down">THE AMPLO BLOG</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 text-center">
                    <div class="row">
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-6.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-5.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>TROY RITCHIE</h5>
                            <p class="role">SR. VP, SALES & MARKETING</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-4.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>EDDIE BEHRINGER</h5>
                            <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>