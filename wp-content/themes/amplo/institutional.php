<?php
/*
Template Name: Institutional
*/
?>

<?php get_header() ?>

    <body class="institution">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>Institutions off all sizes</h1>
                        <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut </span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="large-and-small institution">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <p class="caption">Amplo is for: Institutions </p>
                    <div class="title">
                        <span class="left">LARGE</span>
                        <span class="icon">&</span>
                        <span class="right">SMALL</span>
                    </div>
                    <p class="info">
                        Every institution we work with is at a different place in their advancement efforts. The Amplo Platform was built to provide the tools your institution the full suite of
                        tools you need to be successful.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="carrousel institutional">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme" data-items-on-desktop="4">
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="central-advancement platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Central Advancement: Annual Giving</h2>
                    <p class="info">
                        Annual giving professionals use amplo to save time and raise more money then they ever have online.
                    </p>
                    <h4>Features/ Benefits</h4>
                    <ul>
                        <li>
                            <span>Marketing automation through amplo’s intelligent email platform.</span>
                            <ul>
                                <li>Time Savings: schedule your annual fund email communication and</li>
                            </ul>
                        </li>
                        <li>
                            <span>Online Giving Campaign Management</span>
                            <ul>
                                <li>Time Savings: schedule your annual fund email communication and</li>
                                <li>More Donations: through the most streamlined and intuitive online giving form.</li>
                                <li>Simple Campaign Management: set up the backend and front end of your campaigns in a simple drag and drop manner.</li>
                            </ul>
                        </li>
                    </ul>
                    <div class="text-left">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>COMPLETE ONLINE GIVING</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="alumni-and-constituent platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-6 col-md-6">
                    <h2>Alumni & Constituent Relations</h2>
                    <p class="info">
                        This person uses amplo to connect with alumni digitally and seamlessly manage complex events registrations.
                    </p>
                    <h4>Features/ Benefits</h4>
                    <ul>
                        <li>
                            <span>Fully Integrated Event Registration and Management </span>
                            <ul>
                                <li>Simple: easily set up events that are connected through the same system that is used for giving and email marketing automation.</li>
                                <li>Alumni Directory & Community: deeper alumni engagement .</li>
                                <li>Peer to Peer Modules: powerful peer to peer communications tools that are fully integrated.</li>
                                <li>Volunteer Management:</li>
                            </ul>
                        </li>
                    </ul>
                    <div class="text-left">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>ALUMNI MODULE</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="marketing-and-communication platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>Marketing & Communications</h2>
                    <p class="info">
                        This person uses amplo to send emails, schedule social media, and administer the content management system.
                    </p>
                    <h4>Features/ Benefits</h4>
                    <ul>
                        <li>Content management system: drag and drop interface to live edit webpage (has a gif to insert here of the drag and drop)</li>
                        <li>Intelligent Email Platform: simplicity of collaboration, making getting content of emails easily approved.</li>
                        <li>Powerful Roles: provide access to specific departments to their own email system.</li>
                    </ul>
                    <div class="text-left">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>INTELLIGENT EMAIL DELIVERY PLATFORM"</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>