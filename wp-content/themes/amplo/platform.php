<?php
/*
Template Name: Platform
*/
?>

<?php get_header() ?>

    <body class="platform">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>THE AMPLO PLATFORM</h1>
                        <span>Amplify. Giving Day. Sprout. Unite Alumni Platform</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="complete-suite">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-5 col-md-5">
                    <h2>THE COMPLETE SUITE</h2>
                    <p class="info">
                        The Amplo platforms bring all of your online giving and alumni engagement needs under one simple to use platform.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="one-powerfull">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>ONE POWERFUL AND<br>SIMPLE TO USE PLATFORM</h2>
                    <div class="row">
                        <div class="col-md-10">
                            <p class="info">
                                Leverage one of the stand alone platform modules or take advantage of the Unite All in One Platform
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 items">
                    <div class="item item-1">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-giving-day.jpg') ?>" alt="Giving Day">
                        <h5>Giving Day Platform</h5>
                        <p>Fully integrated and dynamic giving day platform. Paired with the best practices and industry leading strategy.</p>
                    </div>
                    <div class="item main">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-unite-platform.png') ?>" alt="UNIT">
                    </div>
                    <div class="item item-2">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplo.jpg') ?>" alt="AMPLO">
                        <h5>CAMPAIGNS</h5>
                        <p>Digital campaigns are the future of higher education advancement.</p>
                    </div>
                    <div class="item item-3">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.jpg') ?>" alt="Sprout">
                        <h5>sprout CROWDFUNDING</h5>
                        <p>Fully white labeled and Integrated payment processor, application forms, and built on the largest educational based crowdfunding platform in the U.S.</p>
                    </div>
                    <div class="item item-4">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplify.jpg') ?>" alt="AltHere">
                        <h5>AMPLIFY</h5>
                        <p>Peer to Peer Platform, Volunteer Management Module, and CRM in one simple to use platform</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="giving-day-platform platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-center">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-giving-day-2.jpg') ?>" alt="Givin Day">
                        <h2>Giving Day Platform</h2>
                    </div>
                    <h4>Features</h4>
                    <ul>
                        <li> Giving Day Website with Giving Form</li>
                        <li>Multiple Campus Groups</li>
                        <li>Responsible for the Largest Giving Days in Country</li>
                        <li>Dynamic Leaderboard, Challenges, and Matches</li>
                    </ul>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>GIVING DAY TOUR</span></a>
                    </div>
                </div>
                <div class="col-md-3 items">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/giving-day-platform-1.png') ?>" alt="Strategy">
                        <p>The proven strategy and best practices</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/giving-day-platform-2.png') ?>" alt="Freedom">
                        <p>Freedom to choose as many or as little funds needed</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/giving-day-platform-3.png') ?>" alt="Groups">
                        <p>Involve different groups on campus</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="unite platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img src="<?php echo get_theme_file_uri('assets/img/unite-home.png') ?>" alt="Home" class="img-responsive">
                </div>
                <div class="col-md-4 to-down">
                    <div class="text-center">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-unite.png') ?>" alt="Unite">
                        <h2>Unite</h2>
                    </div>
                    <h4>Features</h4>
                    <ul>
                        <li>Complete Online Giving/ Alumni Engagement Website</li>
                        <li>Volunteer Management & Mentorship</li>
                        <li>Alumni Directory/ Community</li>
                        <li>Intelligent Email Platform</li>
                    </ul>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>LEARN MORE</span></a>
                    </div>
                </div>
                <div class="col-md-3 items to-down">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/unite-1.png') ?>" alt="Email Marketing">
                        <p>Save time with an integrated email marketing automation platform</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/unite-1.png') ?>" alt="Events">
                        <p>Steward and solicit donors for giving and events from one place</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/unite-3.png') ?>" alt="Gift">
                        <p>Connect first time donors to your institution, growing for major gift pipeline.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="sprout platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="text-center">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.png') ?>" alt="Sprout">
                        <h2>Sprout</h2>
                    </div>
                    <h4>Features</h4>
                    <ul>
                        <li>Parent company is largest crowdfunding platform in education (Snap!)</li>
                        <li> Peer to Peer Module</li>
                        <li>Easy to use CMS feature</li>
                        <li>Unlimited Projects</li>
                        <li>No Transactional Fees</li>
                    </ul>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>LEARN MORE</span></a>
                    </div>
                </div>
                <div class="col-md-3 items">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/sprout-1.png') ?>" alt="Fast">
                        <p>Fast learning curve with unlimited support and best practice resources</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/sprout-2.png') ?>" alt="Campaigns">
                        <p>Peer to Peer Campaigns</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/sprout-3.png') ?>" alt="Tracking">
                        <p>Robust tracking and volunteer module</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?php echo get_theme_file_uri('assets/img/sprout-desktop.png') ?>" alt="Home" class="img-home">
                </div>
            </div>
        </div>
    </section>

    <section class="amplify unite platform-common-list">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <img src="<?php echo get_theme_file_uri('assets/img/amplify-amplo.png') ?>" alt="Amplify" class="img-responsive">
                </div>
                <div class="col-md-4 to-down">
                    <div class="text-center">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplify-2.png') ?>" alt="Amplify">
                        <h2>AMPLIFY</h2>
                    </div>
                    <h4>Features</h4>
                    <ul>
                        <li>Mobile Responsive with CMS Campaign Page</li>
                        <li>Ambassador Management</li>
                        <li>Intelligent Email Platform</li>
                        <li>Volunteer Management and CRM</li>
                    </ul>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>LEARN MORE</span></a>
                    </div>
                </div>
                <div class="col-md-3 items to-down">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/amplify-1.png') ?>" alt="Easy">
                        <p>Easy to use tools for non-technical ambassadors</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/amplify-2.png') ?>" alt="Campaign">
                        <p>Campaign data that can be used for annual strategy</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/amplify-3.png') ?>" alt="Improved">
                        <p>Improved online giving experience for the donor</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>Consulting Services</h2>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SEE WORK</span></a>
                </div>
                <div class="col-md-offset-2 col-md-5">
                    <h2>Best Practices</h2>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do aeiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>DOWNLOAD WHITEPAPER</span></a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>