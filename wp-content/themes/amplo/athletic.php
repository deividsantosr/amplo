<?php
/*
Template Name: Athletic
*/
?>

<?php get_header() ?>

    <body class="athletic">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>ATHLETICS ADVANCEMENT</h1>
                        <span>Momentum Campaigns. Online Giving. Donor CRM.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="built">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <h2>BUILT FOR ATHLETIC</h2>
                    <p class="info">
                        Capturing the momentum of what your team is doing on the court, field, or diamond shouldn’t be a long and expensive proposition. From rivalry campaigns to complete online
                        giving, Amplo gives you the easiest platform to execute a digital campaign within hours. Once the campaign is complete, our integration with the major ticketing databases
                        make for a perfect union.
                    </p>
                </div>
                <div class="col-md-5">
                    <img src="<?php echo get_theme_file_uri('assets/img/man-built.png') ?>" alt="Built for athletic" class="man">
                </div>
            </div>
        </div>
    </section>

    <section class="advantage-athletic">
        <div class="container">
            <div class="row">
                <div class="col-md-12 items">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/athletic-1.png') ?>" alt="Digital Campaigns">
                        <h5>
                            Simple Digital<br>
                            Campaigns
                        </h5>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/athletic-2.png') ?>" alt="Fully">
                        <h5>
                            Fully integrated<br>
                            crm
                        </h5>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/athletic-3.png') ?>" alt="Marketing">
                        <h5>
                            Marketing<br>
                            Automation
                        </h5>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/athletic-4.png') ?>" alt="Email">
                        <h5>
                            Intelligent<br>
                            Email Platform
                        </h5>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/athletic-5.png') ?>" alt="Online">
                        <h5>
                            Complete<br>
                            Online Giving
                        </h5>
                    </div>
                </div>
            </div>
            <div class="row bottom">
                <div class="col-md-12">
                    <img src="<?php echo get_theme_file_uri('assets/img/athletic-desktop.png') ?>" alt="Desktop">
                </div>
            </div>
        </div>
    </section>

    <section class="air-force">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic-big.png') ?>" alt="Athletic">
                    <p class="percentage"><span class="number">+300</span><span class="icon">%</span></p>
                    <p class="info">
                        Percentage Increase in Dollars<br>
                        Raised based on same<br>
                        Campaign-Year over year!
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>DOWNLOAD CASE STUDY</span></a>
                </div>
                <div class="col-md-6">
                    <h3>the Air force Athletics CASE STUDY</h3>
                    <ul>
                        <li>Previously Air force used multiple vendors/ platforms to administer their operations.</li>
                        <li>Now through Amplo, they currently use 1 platform to successfully use all of the same features. This is at a cost savings from having to use multiple platforms.</li>
                        <li>Key to concept to drive concept is also that we are the millennial digital platform that is needed to solicit and steward donors/ members in this new digital age.</li>
                    </ul>
                    <p class="info">
                        The Air Force Athletics Department utilizes Amplo to operate all the advancement efforts in athletics. This includes:
                    </p>
                    <div class="items">
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/air-force-1.png') ?>" alt="Recursving">
                            <h5>Recurring Giving </h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/air-force-2.png') ?>" alt="Priority">
                            <h5>Priority Points </h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/air-force-3.png') ?>" alt="Online">
                            <h5>Online Giving & Website</h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/air-force-4.png') ?>" alt="Email">
                            <h5>Email Automation</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="large-and-small">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption">Trusted by the Best Athletic <br> Departments that are</p>
                    <div class="title">
                        <span class="left">LARGE</span>
                        <span class="icon">&</span>
                        <span class="right">SMALL</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="integration">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Integrations</h2>
                    <p class="caption">Integrated database & payment processor</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-3 col-md-6 items">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-authorize-net.png') ?>" alt="Authorize.Net">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-elavon.png') ?>" alt="Elavon">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-neulion.png') ?>" alt="Neulion">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-paypal.png') ?>" alt="Paypal">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-cashnet.png') ?>" alt="Cashnet">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-paciolian.png') ?>" alt="Paciolian">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-cyber-source.png') ?>" alt="Cyber Source">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-wepay.png') ?>" alt="Wepay">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/spectra.png') ?>" alt="Spectra">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SEE FULL LIST</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="success-stories">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Success Stories</h2>
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-standford.png') ?>" alt="standford">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="athletic">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-washington.png') ?>" alt="washington">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="longhorn">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu.png') ?>" alt="cmu">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="longhorn">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-washington.png') ?>" alt="washington">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu.png') ?>" alt="cmu">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-standford.png') ?>" alt="standford">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="athletic">
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>