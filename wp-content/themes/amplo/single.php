<?php get_header() ?>

<body class="single">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1><?php echo the_title() ?></h1>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="post-listing single">
        <div class="container">
            <div class="col-md-8">
                <div class="top">
                    <div class="date-and-actions">
                        <p class="author">
                            By <strong>Troy Ritchie</strong>
                        </p>
                        <div class="date">
                            <span><i class="hour"></i> May 3, 1027</span>
                        </div>
                        <div class="actions">
                            <a href="#!"><span class="favorite"></span></a>
                            <a href="#!"><span class="share"></span></a>
                        </div>
                    </div>
                    <div class="tag-list">
                        <a href="#!"><span>Engagement</span></a>
                    </div>
                </div>
                <div class="content">
                    <p>
                        <strong>NOTRE DAME DAY 2017 KICKED</strong> off on Sunday, April 23rd at 6:42 PM EST. Unlike other Giving Days, Notre Dame Day is a global celebration for the entire Notre Dame
                        family to take action and participate.
                    </p>
                    <p>
                        The event started with a live broadcast that would go for 29 hours. The broadcast streamed within our Giving Day platform, featuring exciting stories, live musical
                        performances, and competitions. Facebook live was also a big part of broadcasting the day!
                    </p>
                    <p>
                        Anyone that contributed to Notre Dame Day had their gift converted to votes. To promote equality of gifts among groups, each donor that gave a minimum of $10 received five
                        votes that they could assign to any of the 900+ Area of Interest groups. This way, those with larger philanthropic means could not govern the distribution with large gifts.
                    </p>
                    <p>
                        The 900+ Area of Interest groups all competed for a slice of the $1,000,000 challenge gift. Area of Interest groups included colleges, departments, programs, residence halls,
                        student clubs, Notre Dame clubs, or sports.
                    </p>
                    <p>
                        To keep everyone up to date on the results throughout the day, dynamic leader boards were established in various categories.
                    </p>
                    <img src="<?php echo get_theme_file_uri('assets/img/post-single-1.jpg') ?>" alt="AltHere" class="full">
                    <p>
                        Hourly challenges were also a big part of Notre Dame Day. Groups that receive the most votes during their category’s designated hourly challenge would receive additional funds.
                    </p>
                    <p>
                        “Utilizing our Voting Module is the most effective way to engage young alumni,” Amplo Founder and CEO, Eddie Behringer shared. “Notre Dame does an excellent job providing
                        instruction for anyone giving a gift. It’s such an easy and fun experience. In my opinion, more schools should take advantage of this for their Giving Day, no matter what the
                        size of the institution.”
                    </p>
                    <p>
                        Notre Dame provided videos under their tutorial page that showed everyone exactly how to make a gift and assign their votes.
                    </p>
                    <div class="video">
                        <img src="<?php echo get_theme_file_uri('assets/img/post-single-2.jpg') ?>" alt="AltHere" class="full">
                    </div>
                    <p>
                        Imagery throughout the site reinforced how easy it was to give, cast your vote, and share via social.
                    </p>
                    <img src="<?php echo get_theme_file_uri('assets/img/post-single-3.jpg') ?>" alt="AltHere" class="full">
                    <img src="<?php echo get_theme_file_uri('assets/img/post-single-4.jpg') ?>" alt="AltHere" class="full">
                    <img src="<?php echo get_theme_file_uri('assets/img/post-single-5.jpg') ?>" alt="AltHere" class="full">
                    <p>
                        All throughout the day, sharing results of social ambassadors for each Area of Interest (AOI) group were tracked. An internal leader board incentivized each AOI leader to
                        continue their peer-to-peer sharing efforts.
                    </p>
                    <img src="<?php echo get_theme_file_uri('assets/img/post-single-6.jpg') ?>" alt="AltHere" class="full">
                    <p>
                        As the dust settled on another historic Notre Dame Day, the final numbers read: 25,026 gifts received and $2,175,346 dollars raised. Congratulations to the terrific staff and
                        institution for once again setting the participation record within higher education Giving Days.
                    </p>
                </div>
                <div class="bottom">
                    <div class="recommended-post">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>recomended posts</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="item purple">
                                    <div class="box-image">
                                        <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/recommended-post-1.png') ?>" alt="AltHere"></a>
                                    </div>
                                    <h5><a href="#!">AMPLO CLIENT INSIGHTS: INTERCOM</a></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item purple">
                                    <div class="box-image">
                                        <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/recommended-post-2.png') ?>" alt="AltHere"></a>
                                    </div>
                                    <h5><a href="#!">Our 3:1 Email Marketing Rule</a></h5>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="item">
                                    <div class="box-image">
                                        <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/recommended-post-3.png') ?>" alt="AltHere"></a>
                                    </div>
                                    <h5><a href="#!">GIVING DAY SPOTLIGHT: SEATTLE UNIVERSITY</a></h5>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-schedule-free">
                    <form action="#!" method="post">
                        <h4>SCHEDULE FREE DEMO</h4>
                        <fieldset>
                            <label for="full-name">Full Name</label>
                            <input type="text" id="full-name" name="full-name">
                        </fieldset>
                        <fieldset>
                            <label for="email">Email Adress</label>
                            <input type="email" id="email" name="email">
                        </fieldset>
                        <fieldset>
                            <label for="organization">Intitution/Organization</label>
                            <input type="text" id="organization" name="organization">
                        </fieldset>
                        <fieldset>
                            <label for="phone">Phone Number</label>
                            <input type="text" id="phone" name="phone">
                        </fieldset>
                        <fieldset>
                            <label for="message">How Can Amplo Help Your Institution?</label>
                            <textarea name="message" id="message"></textarea>
                        </fieldset>
                        <button type="submit" class="btn-call-to-action bg-purple"><span>SEND</span></button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-bottom partner careers single">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>SCHEDULE A FREE DEMO</h2>
                    <p class="info">
                        Want to the learn more about the Amplo platforms? Have some specific questions specific to your institution or department? Schedule a live demo to get all your burning
                        questions answered!
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SCHEDULE A DEMO</span></a>
                </div>
            </div>
        </div>
    </section>

    <?php get_footer() ?>
