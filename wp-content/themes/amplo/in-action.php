<?php
/*
Template Name: In Action
*/
?>

<?php get_header() ?>

    <body class="in-action">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>SEE AMPLO IN ACTION</h1>
                        <span>See what's most important to your role or platform you are interested in.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="get-started">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>GET STARTED BY:</h2>
                    <p class="caption">Selecting your Role or Product you are interested in.</p>
                </div>
            </div>
        </div>
    </section>

    <section class="role-and-platform">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="item item-1 title">
                        <h4>Role</h4>
                    </div>
                    <div class="item item-2 title">
                        <h4>Platform</h4>
                    </div>
                    <div class="item item-3">
                        <h5>Annual Giving</h5>
                    </div>
                    <div class="item item-4 right">
                        <h5>Giving Day Platform</h5>
                    </div>
                    <div class="item item-5">
                        <h5>Marketing & Communications</h5>
                    </div>
                    <div class="item item-6 right">
                        <h5>Sprout</h5>
                    </div>
                    <div class="item item-7">
                        <h5>Athletics</h5>
                    </div>
                    <div class="item item-8 right">
                        <h5>Unite</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="role-and-platform-content">
        <div class="container">
            <div class="row initial-msg">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <i class="up"></i>
                    <h2>Click and select the demo you want to watch above</h2>
                </div>
            </div>
            <div class="row carousel hide">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme" data-items-on-desktop="1">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-offset-1 col-md-10">
                                    <div class="row title">
                                        <div class="col-md-12 text-center">
                                            <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.png') ?>" alt="Sprout" class="logo">
                                            <div class="sub">
                                                <h2>Sprout</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row content">
                                        <div class="col-md-7">
                                            <div class="video">
                                                <img src="<?php echo get_theme_file_uri('assets/img/role-and-platform-content-video.png') ?>" alt="Sprout">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h5>Lorem ipsum</h5>
                                            <p class="caption">This demo covers:</p>
                                            <ul>
                                                <li>Parent company is largest crowdfunding platform in education (Snap!)</li>
                                                <li>Peer to Peer Module</li>
                                                <li>Easy to use CMS feature</li>
                                                <li>Unlimited Projects</li>
                                                <li>No Transactional Fees</li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-offset-2 col-md-8">
                                    <div class="row title">
                                        <div class="col-md-12 text-center">
                                            <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.png') ?>" alt="Sprout" class="logo">
                                            <div class="sub">
                                                <p class="caption">
                                                    See the list of a <br>
                                                    vailable videos for
                                                </p>
                                                <h2>Sprout</h2>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row content">
                                        <div class="col-md-7">
                                            <div class="video">
                                                <img src="<?php echo get_theme_file_uri('assets/img/role-and-platform-content-video.png') ?>" alt="Sprout">
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <h5>Lorem ipsum</h5>
                                            <p class="info">
                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>