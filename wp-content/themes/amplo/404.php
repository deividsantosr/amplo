<?php get_header() ?>

<body class="platform">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>ERROR 404</h1>
                        <span>Page not found.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <?php get_footer() ?>
