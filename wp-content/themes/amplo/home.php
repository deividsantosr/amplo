<?php
/*
Template Name: Home
*/
?>

<?php get_header() ?>

    <body class="home">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>HIGHER EDUCATION'S ALL IN ONE</h1>
                        <span>Advancement and Alumni Engagement Platform</span>
                        <a href="#!" class="btn-call-to-action bg-white"><span>SEE WHO AMPLO IS FOR</span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="giving-day">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="text-center">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-notre-dame.png') ?>" alt="Notre Dame">
                    </div>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet orci hendrerit, fringilla ipsum eu, mollis augue. Sed ac est vitae arcu porta laoreet. Nullam nec
                        lobortis mauris.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-alpha"><span>GIVING DAY</span></a>
                    </div>
                </div>
                <div class="col-md-7">
                    <h2>University of Notre Dame</h2>
                    <p class="caption">22000 gifts in 30 hours</p>
                    <video width="460" height="260" controls>
                        <source src="<?php echo get_theme_file_uri('assets/video/university-of-notre-dame.mp4') ?>" type="video/mp4">
                    </video>
                </div>
            </div>
        </div>
    </section>

    <section class="institutional-size">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>Institution <br>of all sizes</h2>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet orci hendrerit, fringilla ipsum eu, mollis augue. Sed ac est vitae arcu porta laoreet. Nullam nec
                        lobortis mauris. Sed nec dignissim sapien. Sed ligula nisi, vehicula et imperdiet vitae, semper sit amet ante. Ut ac lacus vulputate enim vehicula egestas.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>SCHEDULE A DEMO</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="overview">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <img src="<?php echo get_theme_file_uri('assets/img/overview.jpg') ?>" alt="Overview" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <h2>OVERVIEW</h2>
                    <a href="#!" class="btn-call-to-action bg-white border-gray"><span>PLATFORM</span></a>
                    <p class="info">
                        Nam vitae sem nec nibh tempus tempus. Nullam id ex nisl. Donec vestibulum mauris eu turpis malesuada eleifend. Cras vitae tellus dolor. Etiam dapibus nulla scelerisque
                        lorem
                        molestie, et mattis orci consectetur. Pellentesque facilisis purus ac ligula elementum interdum. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc at semper
                        sem.
                        Aliquam ut justo quis elit vulputate auctor nec in sapien. Ut in velit id ligula sagittis mattis luctus elementum nulla. Mauris a elementum urna, a luctus libero. Nulla
                        venenatis porta porttitor. Vestibulum pharetra enim a ex sodales.
                    </p>
                    <a href="#!" class="btn-call-to-action bg-purple"><span>READ MORE</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="features">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="item">
                        <a href="#!" class="btn-call-to-action bg-white border-gray"><span>FEATURES</span></a>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/acquire.png') ?>" alt="Acquire">
                        <h5>ACQUIRE</h5>
                        <p>New & Lapsed Donors Through Powerful Technoloogy</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/qualify.png') ?>" alt="Qualify">
                        <h5>QUALIFY</h5>
                        <p>Focus on The Right People & Connect With The Right Message</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/integrate.png') ?>" alt="Integrate">
                        <h5>INTEGRATE</h5>
                        <p>Existing Systems & Amplo to Leverage Data Like Never Before</p>
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/icon/convert.png') ?>" alt="Convert">
                        <h5>CONVERT</h5>
                        <p>Through Intelligent Stewardship & The Right Digital Message</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="modules">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-advancement.png') ?>" alt="Advancement" class="module-1">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-marketing.png') ?>" alt="Marketing" class="module-2">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-alumni.png') ?>" alt="Alumni" class="module-3">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <a href="#!" class="btn-call-to-action bg-white border-gray"><span>MODULES</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 items">
                            <div class="item">
                                <h5>CAMPAIGNS</h5>
                                <p>
                                    Giving Day <br>
                                    Fully Integrated Crowdfunding <br>
                                    Peer to Peer
                                </p>
                            </div>
                            <div class="item">
                                <h5>ONLINE GIVING</h5>
                                <p>
                                    Dynamic Giving Forms <br>
                                    Intelligent Email Platform <br>
                                    Marketing Automation
                                </p>
                            </div>
                            <div class="item">
                                <h5>ALUMNI ENGAGEMENT</h5>
                                <p>
                                    Event Registration <br>
                                    P2P Module <br>
                                    Alumni Directory <br>
                                    Volunteer Management
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="institutions">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Institutions</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="item">
                        <div class="box-image">
                            <img src="<?php echo get_theme_file_uri('assets/img/logo-standford.png') ?>" alt="Standford">
                        </div>
                        <h6>The Stanford Fund</h6>
                        <p>
                            The Stanford Fund uses Amplo's fully integrated crowdsourced features to connect donors to the impact of their gifts. They recognize the importance of delivering their
                            message properly. Multiple campus units from Young Alumni, Central Advancement, Senior Gift, and Reunion Initiatives use Amplo.
                        </p>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="Athletic">
                        </div>
                        <h6>Athletic Department</h6>
                        <p>
                            The US Air Force Academy raised <strong>$60,000 in 1 week</strong>, by providing a small group of supporters simple and easy online sharing tools. The USAFA utilizes
                            the Amplo's fully
                            integrated donor management tools that provide key data points, through a simple and easy to use interface.
                        </p>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <img src="<?php echo get_theme_file_uri('assets/img/logo-washington.png') ?>" alt="Whashington">
                        </div>
                        <h6>Public State University</h6>
                        <p>
                            The US Air Force Academy raised <strong>$60,000 in 1 week</strong>, by providing a small group of supporters simple and easy online sharing tools. The USAFA utilizes
                            the Amplo's fully integrated donor management tools that provide key data points, through a simple and easy to use interface.
                        </p>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="Longhorn">
                        </div>
                        <h6>Athletic Foundation</h6>
                        <p>
                            The Longhorn Foundation’s first ever crowdfunding campaign will strive to provide a scholarship for a tremendous student-athlete. The University of Texas also leverages
                            the platform for fully integrated Annual Fund & Letterwinner Campaign Initiatives.
                        </p>
                    </div>
                    <div class="item">
                        <div class="box-image">
                            <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu.png') ?>" alt="CMU">
                        </div>
                        <h6>Central methodist Uni</h6>
                        <p>
                            The Longhorn Foundation’s first ever crowdfunding campaign will strive to provide a scholarship for a tremendous student-athlete. The University of Texas also leverages
                            the platform for fully integrated Annual Fund & Letterwinner Campaign Initiatives.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="#!" class="btn-call-to-action bg-white text-gray"><span>SEE OTHERS CLIENTS</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="athletics">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-5 col-md-7">
                    <h2>ATHLETICS ADVANCEMENT</h2>
                    <p class="caption">
                        Trusted by Athletic Departments of All Sizes
                    </p>
                    <div class="row">
                        <div class="col-md-8">
                            <p class="info">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sit amet orci hendrerit, fringilla ipsum eu, mollis augue. Sed ac est vitae arcu porta laoreet.
                                Nullam nec
                                lobortis mauris. Sed nec dignissim sapien. Sed ligula nisi, vehicula et imperdiet vitae, semper sit amet ante. Ut ac lacus vulputate enim vehicula egestas.
                            </p>
                            <div class="text-center">
                                <a href="#!" class="btn-call-to-action bg-alpha"><span>READ MORE</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>