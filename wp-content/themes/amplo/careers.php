<?php
/*
Template Name: Careers
*/
?>

<?php get_header() ?>

    <body class="careers">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>Careers</h1>
                        <span>We continue to grow and bring our fundraising platform across the country. We are looking for talented people that can help in realizing our vision of bringing a simple and powerful fundraising solution to those that need it most. Have passion, talent, and initiative? Let’s talk.</span>
                        <a href="#!" class="btn-call-to-action bg-alpha"><span>OPEN POSITIONS</span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="large-and-small partner">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <div class="title">
                        <span class="left">HUMBLE</span>
                        <span class="icon">&</span>
                        <span class="right">HUNGRY</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="apply-for-position">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>APPLY FOR OPEN POSITIONS</h2>
                    <h3>Why is this Opportunity Unique?</h3>
                    <p class="info">
                        Define your process and foster ownership over our team's technical success Work on software that gets deployed to a massive audience on a large scale. Contribute to a software
                        solution that makes a lasting impact in the communities we work with, year after year.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h4>FRONT END DEVELOPER</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        We are looking for a versatile technical addition to our development team here in Seattle. The position offers an extensive amount of ownership and influence to our development
                        processes as we scale the largest team based fundraising solution in the country.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>CLIENT SERVICES</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        We are looking for an experienced client services/ customer success professional to join our team! If you are passionate about the customer experience and advocating for their
                        success, this role is for you!
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>DEV OPS</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        The position offers an extensive amount of ownership and influence to our development processes as we scale the largest team based fundraising solution in the country.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="mission-statement">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <h2>MISSION STATEMENT</h2>
                    <p class="caption">Fully Deployed</p>
                    <p class="info">
                        We are a young, exciting new start-up that is delivering an innovative SAAS platform to the college and high school sports teams market, that helps schools and non-profits fund
                        raise more effectively. When we say more effectively, we mean dramatically-our average customer raises 400% more with our funding technologies than previous drives they have
                        done. We foster a great development atmosphere where creative license is not just offered but required as we revolutionize theway groups fund raise. Oh, and did we mention that
                        we have a company Kickball, Softball, and Dodgeball Team?!
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="our-culture">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>OUR CULTURE</h2>
                    <p class="info">
                        Our Ideology as we Build, Create, and Innovate <br><br>
                        Our workflow is cut, dry, and efficient. Adaptation is key, can you join this process?
                        We only work on the most important things at any given point in time. We then break those things into the smaller manageable tasks for individuals to work on autonomously.
                        Individual's strong suits are considered in assigning tasks and accomplishing the collective team goal. Every morning the team recaps progress and blocking points for about
                        10-20 minutes, directed by the team lead. Communication is the upmost of importance, are you comfortable asking for help, and open to accepting constructive feedback? Formally
                        and collectively understand once a MVP sprint has been completed, to reevaluate the next set of priorities. Sometimes as frequently as weekly.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <section class="perks">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>PERKS</h2>
                    <div class="items">
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/perks-1.png') ?>" alt="Vacation">
                            <h5>PAID VACATION</h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/perks-2.png') ?>" alt="Setup">
                            <h5>dream setup</h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/perks-3.png') ?>" alt="CO-Workers">
                            <h5>Awesome <br>co-workers</h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/perks-4.png') ?>" alt="Sports">
                            <h5>Free Sports Leage entrance: yes kickball!</h5>
                        </div>
                        <div class="item">
                            <img src="<?php echo get_theme_file_uri('assets/img/icon/perks-5.png') ?>" alt="Health">
                            <h5>Health Benefits</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>