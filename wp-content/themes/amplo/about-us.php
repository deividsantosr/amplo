<?php
/*
Template Name: About Us
*/
?>

<?php get_header() ?>

    <body class="about-us">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>About Us</h1>
                        <span>We started by building the largest educational based crowfunding platform in the U.S. We are now shaping Higher Education’s Digital Fundraing Landscape.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="apply-for-position">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h2>Shaping the digital fundraising & alumini engagement landscape</h2>
                    <p class="info">
                        Define your process and foster ownership over our team's technical success Work on software that gets deployed to a massive audience on a large scale. Contribute to a software
                        solution that makes a lasting impact in the communities we work with, year after year.
                    </p>
                    <h3 class="small">Out team includes industry experts who have helped groups or all sizes achieve their fundraising goals.</h3>
                    <p class="info">
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis esse quisquam sapiente. Consequuntur dignissimos error, incidunt laboriosam pariatur qui voluptates! Accusamus
                        architecto ipsa necessitatibus ratione? Dolore enim hic quaerat repellat. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab architecto atque commodi debitis dolore
                        exercitationem minima ratione voluptas voluptate voluptatem! Cum debitis doloribus dolorum ex inventore ipsam quia sit! Magnam! Lorem ipsum dolor sit amet, consectetur
                        adipisicing elit. Cupiditate, dignissimos illum impedit maxime non unde vel. Autem consequatur, ea error illum in modi nobis quas quod tempora! Asperiores, error, magni?
                    </p>
                </div>
                <div class="col-md-4 text-center">
                    <div class="count">
                        <span class="title"><i class="icon">$</i>101,639,206</span>
                        <span class="caption">$100M+ Dollars raised online</span>
                    </div>
                    <div class="count">
                        <span class="title"><i class="icon">$</i>2,646,962</span>
                        <span class="caption">2.60M+ Users on our platform</span>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile odd">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>MEET OUR TEAM</h2>
                    <p class="caption">LEADERSHIP</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-1.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>COLE MORGAN</h5>
                            <p class="role">CO-FOUNDER-SALES</p>
                            <p class="info">
                                Cole is responsible for directing Sales. Having personally raised groups several Million Dollars he brings experience and enthusiasm to Amplo’s Development. By working
                                directly with institutions of all sizes, Cole is able to provide customized solutions that allow the institution to drive the greatest return.
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>EDDIE BEHRINGER</h5>
                            <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                            <p class="info">
                                Eddie is in charge of Product Development. He is well versed in bringing large product visions to technical success. By bridging the gap between the real world issues &
                                donor analytics that higher education platforms have not addressed for decades; he is able to put these powerful features at your institution’s finger tips.
                            </p>
                        </div>
                        <div class="col-md-4">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-3.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>STEFAN BERLGUND</h5>
                            <p class="role">CO-FOUNDER-FINANCE</p>
                            <p class="info">
                                Stefan was a founding member and is responsible for business finance, creating exemplary operation systems, and ensuring great customer service. Stefan works tirelessly
                                to perfect the fundraising process and find ways in which groups can be more efficient and raise more. He genuinely cares about raising money for groups and ensuring
                                that keep the largest portion of fundraising proceeds as possible.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile even">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption">SALES / MARKETING</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <div class="row">
                        <div class="col-md-4 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-4.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                            <p class="info">
                                Jason came to Amplo in Seattle by way of Charleston, South Carolina. As a former Publisher for Arcadia Publishing, Jason leads the project and account management team
                                at Amplo. Jason is a graduate of the College of Charleston where he is a founding member of the CofC Hockey Team.
                            </p>
                        </div>
                        <div class="col-md-4 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-5.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>TROY RITCHIE</h5>
                            <p class="role">SR. VP, SALES & MARKETING</p>
                            <p class="info">
                                Troy heads up all of Sales & Marketing for Amplo and brings with him over 20 years’ experience in fan engagement within Sports and Entertainment. With a strong
                                knowledge of email marketing and database management he has helped several clients like Philips Healthcare and Microsoft make better data driven decisions.
                            </p>
                        </div>
                        <div class="col-md-4 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-6.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>JUSTIN SKOLRUD</h5>
                            <p class="role">INSIDE SALES & DIGITAL MARKETING SPECIALIST</p>
                            <p class="info">
                                Inside Sales & Digital Marketing Specialist
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile odd">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption">CLIENT SERVICES</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 text-center">
                    <div class="row">
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-1.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>COLE MORGAN</h5>
                            <p class="role">CO-FOUNDER-SALES</p>
                            <p class="info">
                                Cole is responsible for directing Sales. Having personally raised groups several Million Dollars he brings experience and enthusiasm to Amplo’s Development. By working
                                directly with institutions of all sizes, Cole is able to provide customized solutions that allow the institution to drive the greatest return.
                            </p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>EDDIE BEHRINGER</h5>
                            <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                            <p class="info">
                                Eddie is in charge of Product Development. He is well versed in bringing large product visions to technical success. By bridging the gap between the real world issues &
                                donor analytics that higher education platforms have not addressed for decades; he is able to put these powerful features at your institution’s finger tips.
                            </p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-3.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>STEFAN BERLGUND</h5>
                            <p class="role">CO-FOUNDER-FINANCE</p>
                            <p class="info">
                                Stefan was a founding member and is responsible for business finance, creating exemplary operation systems, and ensuring great customer service. Stefan works tirelessly
                                to perfect the fundraising process and find ways in which groups can be more efficient and raise more. He genuinely cares about raising money for groups and ensuring
                                that keep the largest portion of fundraising proceeds as possible.
                            </p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-6.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                            <p class="info">
                                Jason came to Amplo in Seattle by way of Charleston, South Carolina. As a former Publisher for Arcadia Publishing, Jason leads the project and account management team
                                at Amplo. Jason is a graduate of the College of Charleston where he is a founding member of the CofC Hockey Team.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile even">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption">ENGINEERING</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 text-center">
                    <div class="row">
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-1.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>COLE MORGAN</h5>
                            <p class="role">CO-FOUNDER-SALES</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>EDDIE BEHRINGER</h5>
                            <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-3.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>STEFAN BERLGUND</h5>
                            <p class="role">CO-FOUNDER-FINANCE</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-6.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-1.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>COLE MORGAN</h5>
                            <p class="role">CO-FOUNDER-SALES</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>EDDIE BEHRINGER</h5>
                            <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-3.png') ?>" alt="">
                                </div>
                                <i class="icon"></i>
                            </div>
                            <h5>STEFAN BERLGUND</h5>
                            <p class="role">CO-FOUNDER-FINANCE</p>
                        </div>
                        <div class="col-md-3 item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-6.png') ?>" alt="">
                                </div>
                                <i class="icon active"></i>
                            </div>
                            <h5>JASON HUMPHREY</h5>
                            <p class="role">DIRECTOR-CLIENT SERVICES</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="linkedin-profile odd">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <p class="caption">STRATEGY</p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-2 col-md-8 text-center">
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8">
                            <div class="row">
                                <div class="col-md-6 item">
                                    <div class="box-image">
                                        <div class="image">
                                            <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-1.png') ?>" alt="">
                                        </div>
                                        <i class="icon active"></i>
                                    </div>
                                    <h5>COLE MORGAN</h5>
                                    <p class="role">CO-FOUNDER-SALES</p>
                                    <p class="info">
                                        Cole is responsible for directing Sales. Having personally raised groups several Million Dollars he brings experience and enthusiasm to Amplo’s Development. By
                                        working
                                        directly with institutions of all sizes, Cole is able to provide customized solutions that allow the institution to drive the greatest return.
                                    </p>
                                </div>
                                <div class="col-md-6 item">
                                    <div class="box-image">
                                        <div class="image">
                                            <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-2.png') ?>" alt="">
                                        </div>
                                        <i class="icon active"></i>
                                    </div>
                                    <h5>EDDIE BEHRINGER</h5>
                                    <p class="role">FOUNDER-CHIEF TECHNICAL OFFICER</p>
                                    <p class="info">
                                        Eddie is in charge of Product Development. He is well versed in bringing large product visions to technical success. By bridging the gap between the real world
                                        issues &
                                        donor analytics that higher education platforms have not addressed for decades; he is able to put these powerful features at your institution’s finger tips.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="apply-for-position">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>APPLY FOR OPEN POSITIONS</h2>
                    <h3>Why is this Opportunity Unique?</h3>
                    <p class="info">
                        Define your process and foster ownership over our team's technical success Work on software that gets deployed to a massive audience on a large scale. Contribute to a software
                        solution that makes a lasting impact in the communities we work with, year after year.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <h4>FRONT END DEVELOPER</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        We are looking for a versatile technical addition to our development team here in Seattle. The position offers an extensive amount of ownership and influence to our development
                        processes as we scale the largest team based fundraising solution in the country.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>CLIENT SERVICES</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        We are looking for an experienced client services/ customer success professional to join our team! If you are passionate about the customer experience and advocating for their
                        success, this role is for you!
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4>DEV OPS</h4>
                    <p class="caption">Seattle, Washington, USA</p>
                    <p class="text">
                        The position offers an extensive amount of ownership and influence to our development processes as we scale the largest team based fundraising solution in the country.
                    </p>
                    <div class="text-center">
                        <a href="#!" class="btn-call-to-action bg-purple"><span>APPLY FOR THIS POSITION</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-bottom partner careers">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>SCHEDULE A FREE DEMO</h2>
                    <p class="info">
                        Want to the learn more about the Amplo platforms? Have some specific questions specific to your institution or department? Schedule a live demo to get all your burning
                        questions answered!
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SCHEDULE A DEMO</span></a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>