<?php
/*
Template Name: Partner
*/
?>

<?php get_header() ?>

    <body class="partner">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>AMPLO PARTNERS</h1>
                        <span>Amplo partners with institutions of all sizes to achieve their goals. View a sampling of a few of our greatest hits below.</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="list-logo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 items">
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-ou.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-stanclient.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-gonzaga.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-idaho-state.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu-small.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-nd.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-vanderbilt.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-mira-costa.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-ou.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-stanclient.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-gonzaga.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-idaho-state.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu-small.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-nd.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-vanderbilt.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-mira-costa.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-ou.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-stanclient.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-longhorn.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="">
                    </div>
                    <div class="item">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-gonzaga.png') ?>" alt="">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SEE PLATFORM IN ACTION</span></a>
                </div>
            </div>
        </div>
    </section>

    <section class="carrousel institutional partner">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme" data-items-on-desktop="3">
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <p class="caption">Providence college</p>
                            <h5>Friars Give Day Campaign</h5>
                            <div class="box-image">
                                <img src="<?php echo get_theme_file_uri('assets/img/carrousel-default.jpg') ?>" alt="Video">
                            </div>
                            <div class="info">
                                <div class="left">
                                    We accomplished our goal of reaching at least 1,917 donors and $100,000 in donations. With more than 3,000 donors making over $600,000 in gifts your generosity
                                    will
                                    help catapult PC into its second century.
                                </div>
                                <div class="right">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-providence.png') ?>" alt="Providence" class="brand">
                                    <p class="goals">
                                        <span class="caption">Original goal</span>
                                        <span class="value">$100k</span>
                                    </p>
                                    <p class="goals raised">
                                        <span class="caption">with amplo raised</span>
                                        <span class="value"><strong>$700k</strong></span>
                                    </p>
                                </div>
                                <div class="bottom text-center">
                                    <a href="#!" class="btn-call-to-action bg-white border-blue"><span>visit website campaign</span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="one-powerfull partner">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>ONE POWERFUL AND<br>SIMPLE TO USE PLATFORM</h2>
                    <div class="row">
                        <div class="col-md-10">
                            <p class="info">
                                Leverage one of the stand alone platform modules or take advantage of the Unite All in One Platform
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 items">
                    <div class="item item-1">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-giving-day.jpg') ?>" alt="Giving Day">
                        <h5>Giving Day Platform</h5>
                        <p>Fully integrated and dynamic giving day platform. Paired with the best practices and industry leading strategy.</p>
                    </div>
                    <div class="item main">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-unite-platform.png') ?>" alt="UNIT">
                    </div>
                    <div class="item item-2">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplo.jpg') ?>" alt="AMPLO">
                        <h5>CAMPAIGNS</h5>
                        <p>Digital campaigns are the future of higher education advancement.</p>
                    </div>
                    <div class="item item-3">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.jpg') ?>" alt="Sprout">
                        <h5>sprout CROWDFUNDING</h5>
                        <p>Fully white labeled and Integrated payment processor, application forms, and built on the largest educational based crowdfunding platform in the U.S.</p>
                    </div>
                    <div class="item item-4">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplify.jpg') ?>" alt="AltHere">
                        <h5>AMPLIFY</h5>
                        <p>Peer to Peer Platform, Volunteer Management Module, and CRM in one simple to use platform</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="platform-bottom partner">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <h2>SCHEDULE A FREE DEMO</h2>
                    <p class="info">
                        Want to the learn more about the Amplo platforms? Have some specific questions specific to your institution or department? Schedule a live demo to get all your burning
                        questions answered!
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span>SCHEDULE A DEMO</span></a>
                </div>
            </div>
        </div>
    </section>


<?php get_footer(); ?>