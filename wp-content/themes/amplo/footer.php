<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h5>Schedule a demo</h5>
                <ul>
                    <li><a href="<?php echo get_site_url() ?>/platform">Platform</a></li>
                    <li><a href="<?php echo get_site_url() ?>/institutional">Who amplo is for</a></li>
                    <li><a href="<?php echo get_site_url() ?>/athletic">Athletics DEPARTMENTS</a></li>
                    <li><a href="<?php echo get_site_url() ?>/see-amplo-in-action/">Demo</a></li>
                    <li><a href="<?php echo get_site_url() ?>/partner">Clients</a></li>
                    <li><a href="<?php echo get_site_url() ?>/blog">Blog</a></li>
                    <li><a href="<?php echo get_site_url() ?>/about-us">About us</a></li>
                    <li><a href="<?php echo get_site_url() ?>">Contact</a></li>
                    <li><a href="<?php echo get_site_url() ?>">Giving day</a></li>
                    <li><a href="<?php echo get_site_url() ?>">Alumni</a></li>
                </ul>
            </div>
            <div class="col-md-4 text-center">
                <a href="<?php echo get_home_url() ?>">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo.png') ?>" alt="AMPLO" class="logo">
                </a>
                <h4>CONTACT US</h4>
                <address>
                    939 Westlake Ave. N. <br>
                    Seattle, WA 98109
                </address>
                <address>
                    <a href="mailto:support@amploadvance.com">support@amploadvance.com</a> <br>
                    <a href="tel:1-888-963-9616">1-888-963-9616</a>
                </address>
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-offset-3 col-md-6 text-center">
                        <p>
                            Amplo is the Next Generation Platform Built Specifically for Higher Education. Leverage the Latest Technologies to Seamlessly Steward Donors and Alumni.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="copyright text-center">© Amplo Advance 2017. All right reserved.</p>
            </div>
        </div>
    </div>
</footer>

<script type="application/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="application/javascript" src="<?php echo get_theme_file_uri('assets/js/main.js') ?>"></script>
<script type="application/javascript" src="<?php echo get_theme_file_uri('assets/libs/owl-carousel-2-2.2.1/owl.carousel.min.js') ?>"></script>
</body>
</html>