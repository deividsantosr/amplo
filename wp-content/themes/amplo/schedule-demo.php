<?php
/*
Template Name: Schedule Demo
*/
?>

<?php get_header() ?>

    <body class="schedule-demo">
    <header>
        <?php get_template_part('template-parts/header/top-header') ?>

        <div class="middle-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 text-center">
                        <h1>Schedule a demo</h1>
                        <span>See how other institutions like yours are leveraging the Amplo PLatform.</span>
                        <a href="#!" class="btn-call-to-action bg-purple"><span>SCHEDULE A DEMO</span></a>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <section class="one-powerfull schedule-demo">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h2>ONE POWERFUL AND<br>SIMPLE TO USE PLATFORM</h2>
                    <div class="row">
                        <div class="col-md-10">
                            <p class="info">
                                Leverage one of the stand alone platform modules or take advantage of the Unite All in One Platform
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 items">
                    <div class="item item-1">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-giving-day.jpg') ?>" alt="Giving Day">
                        <h5>Giving Day Platform</h5>
                        <p>Fully integrated and dynamic giving day platform. Paired with the best practices and industry leading strategy.</p>
                    </div>
                    <div class="item main">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-unite-platform.png') ?>" alt="UNIT">
                    </div>
                    <div class="item item-2">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplo.jpg') ?>" alt="AMPLO">
                        <h5>CAMPAIGNS</h5>
                        <p>Digital campaigns are the future of higher education advancement.</p>
                    </div>
                    <div class="item item-3">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-sprout.jpg') ?>" alt="Sprout">
                        <h5>sprout CROWDFUNDING</h5>
                        <p>Fully white labeled and Integrated payment processor, application forms, and built on the largest educational based crowdfunding platform in the U.S.</p>
                    </div>
                    <div class="item item-4">
                        <img src="<?php echo get_theme_file_uri('assets/img/logo-amplify.jpg') ?>" alt="AltHere">
                        <h5>AMPLIFY</h5>
                        <p>Peer to Peer Platform, Volunteer Management Module, and CRM in one simple to use platform</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="prefer-more">
        <div class="container">
            <div class="row">
                <div class="col-md-5 left text-center">
                    <h2>prefer a more<br>personal demo?</h2>
                    <p class="info">
                        Ipsam aute minus aliquet duis ridiculus excepturi dicta exercitation, molestiae, placeat reprehenderit laudantium quis taciti nunc, debitis integer aenean ad ea excepteur.
                        <br><br>
                        Ipsam aute minus aliquet duis ridiculus excepturi dicta exercitation, molestiae, placeat reprehenderit laudantium quis taciti
                    </p>
                    <a href="#!" class="btn-call-to-action bg-alpha"><span><i class="schedule"></i>SCHEDULE A DEMO</span></a>
                </div>
                <div class="col-md-offset-2 col-md-5 right text-center">
                    <h2>or Try demo</h2>
                    <p class="info">
                        Ipsam aute minus aliquet duis ridiculus excepturi dicta exercitation, molestiae.
                    </p>
                    <form action="#!" method="post">
                        <fieldset class="check">
                            <input type="text" name="full-name" placeholder="John Smith">
                        </fieldset>
                        <fieldset>
                            <input type="email" name="email" placeholder="Email">
                        </fieldset>
                        <fieldset>
                            <input type="text" name="phone" placeholder="Phone Number">
                        </fieldset>
                        <button type="submit" class="btn-call-to-action bg-white"><span>GO TO DEMO</span></button>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="modules schedule-demo">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-advancement.png') ?>" alt="Advancement" class="module-1">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-marketing.png') ?>" alt="Marketing" class="module-2">
                    <img src="<?php echo get_theme_file_uri('assets/img/module-alumni.png') ?>" alt="Alumni" class="module-3">
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-6 text-center">
                            <a href="#!" class="btn-call-to-action bg-white border-gray"><span>MODULES</span></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 items">
                            <div class="item">
                                <h5>CAMPAIGNS</h5>
                                <p>
                                    Giving Day <br>
                                    Fully Integrated Crowdfunding <br>
                                    Peer to Peer
                                </p>
                            </div>
                            <div class="item">
                                <h5>ONLINE GIVING</h5>
                                <p>
                                    Dynamic Giving Forms <br>
                                    Intelligent Email Platform <br>
                                    Marketing Automation
                                </p>
                            </div>
                            <div class="item">
                                <h5>ALUMNI ENGAGEMENT</h5>
                                <p>
                                    Event Registration <br>
                                    P2P Module <br>
                                    Alumni Directory <br>
                                    Volunteer Management
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="linkedin-profile even testimonials">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>Testimonials</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 text-center">
                    <div class="owl-carousel owl-theme" data-items-on-desktop="3">
                        <div class="item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-7.png') ?>" alt="">
                                </div>
                                <div class="company">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-athletic.png') ?>" alt="Athletic">
                                </div>
                            </div>
                            <h5>Terry Johnson</h5>
                            <p class="role">DIRECTOR <br> Central washington University</p>
                            <p class="info">
                                “Lectus leo morbi veritatis dolore nesciunt, hymenaeos. Massa, repellat iste nullam habitasse, proin! Parturient. Inceptos porttitor nostrum, pharetra, leo, laboriosam,
                                ipsa ligula ducimus repellat ultrices neque. Aenean sunt totam incidunt senectus per aut quia! Nulla proin? Augue adipisicing, nullam id.”
                            </p>
                        </div>
                        <div class="item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-8.png') ?>" alt="">
                                </div>
                                <div class="company">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-washington.png') ?>" alt="Washington">
                                </div>
                            </div>
                            <h5>JASON Nielsen</h5>
                            <p class="role">DIRECTOR <br> Central washington University</p>
                            <p class="info">
                                “Lectus leo morbi veritatis dolore nesciunt, hymenaeos. Massa, repellat iste nullam habitasse, proin! Parturient. Inceptos porttitor nostrum, pharetra, leo, laboriosam,
                                ipsa ligula ducimus repellat ultrices neque. Aenean sunt totam incidunt senectus per aut quia! Nulla proin? Augue adipisicing, nullam id.”
                            </p>
                        </div>
                        <div class="item">
                            <div class="box-image">
                                <div class="image">
                                    <img src="<?php echo get_theme_file_uri('assets/img/linkedin-profile-9.png') ?>" alt="">
                                </div>
                                <div class="company large">
                                    <img src="<?php echo get_theme_file_uri('assets/img/logo-cmu.png') ?>" alt="CMU">
                                </div>
                            </div>
                            <h5>Jeffrey Jonnes</h5>
                            <p class="role">DIRECTOR <br> Central Methodist University</p>
                            <p class="info">
                                “Lectus leo morbi veritatis dolore nesciunt, hymenaeos. Massa, repellat iste nullam habitasse, proin! Parturient. Inceptos porttitor nostrum, pharetra, leo, laboriosam,
                                ipsa ligula ducimus repellat ultrices neque. Aenean sunt totam incidunt senectus per aut quia! Nulla proin? Augue adipisicing, nullam id.”
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="post-listing schedule-demo">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2>newest articles</h2>
                </div>
            </div>
            <div class="row list">
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-2.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>CLIENT BEST PRATICES</span></a>
                                <a href="#!"><span>TECHNICAL FAQ</span></a>
                            </div>
                            <h3><a href="#!">AMPLO CLIENT INSIGHTS: INTERCOM</a></h3>
                            <p class="summary">
                                Amplo is proud to launch a brand-new feature, Intercom, that will streamline client support interaction. Intercom will be the center for communication, tracking, and
                                receiving information for all your platform needs. Whether it’s a question on managing a campaign, on overall best practices, on something that isn’t quite working for
                                you, or that you have a new idea for how we can improve the platform, Intercom will be the tool to assist you.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-3.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>Engagement</span></a>
                            </div>
                            <h3><a href="#!">Our 3:1 Email Marketing Rule</a></h3>
                            <p class="summary">
                                An excerpt from our Giving Day Handbook: <br><br>
                                Giving Day Team Structure <br><br>
                                Once the Giving Day goals have been put in place, create your overall strategy by building a team to accomplish these goals. In working with leadership, make sure they
                                are aware of the key milestones you are looking to accomplish. As you grow, your team may have varying opinions. Scheduling one to two brainstorming sessions within the
                                planning period will ensure everyone is on the same page.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="item">
                        <div class="box-image">
                            <a href="#!"><img src="<?php echo get_theme_file_uri('assets/img/post-list-4.jpg') ?>" alt="Title"></a>
                        </div>
                        <div class="box-info">
                            <div class="tag-list">
                                <a href="#!"><span>giving day</span></a>
                            </div>
                            <h3><a href="#!">GIVING DAY SPOTLIGHT: SEATTLE UNIVERSITY</a></h3>
                            <p class="summary">
                                Amplo, located in Seattle, WA, is proud to partner and launch our first local giving day with Seattle U! The campaign, Seattle U Gives, is the very first giving day
                                being run by SU.
                            </p>
                            <div class="bottom date-and-actions">
                                <div class="date">
                                    <span><i class="hour"></i> May 3, 1027</span>
                                </div>
                                <div class="actions">
                                    <a href="#!"><span class="favorite"></span></a>
                                    <a href="#!"><span class="share"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row load-more">
                <div class="col-md-12">
                    <a href="<?php echo get_site_url() ?>/blog" class="btn-call-to-action bg-purple"><span>GO TO BLOG</span></a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>