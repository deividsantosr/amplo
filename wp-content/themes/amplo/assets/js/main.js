$(document).ready(function () {
    var app = {
        init: function () {
            menuMobile.init();
            owlCarousel.init();
            roleAndPlatform.init();
        }
    };

    var menuMobile = {
        selector: 'header .top-header .menu .menu-mobile',

        init: function () {
            this.element = $(this.selector);

            this.bindEvents();
        },

        bindEvents: function () {
            this.element.bind('click', this.toggleMenu);
        },

        toggleMenu: function () {
            $(this).toggleClass('open');
        }
    };

    var owlCarousel = {
        selector: '.owl-carousel',

        itemsOnDesktop: 1,

        init: function () {
            this.element = $(this.selector);

            if (this.element.data('items-on-desktop') != undefined) {
                this.itemsOnDesktop = parseInt(owlCarousel.element.data('items-on-desktop'));
            }

            if (this.element.length) {
                this.bindEvents();
            }
        },

        bindEvents: function () {
            this.element.owlCarousel({
                loop: true,
                margin: 20,
                nav: true,
                dots: true,
                responsive: {
                    0: {
                        items: 1
                    },
                    1000: {
                        items: owlCarousel.itemsOnDesktop
                    }
                }
            });

            this.element.on('translated.owl.carousel', function () {
                var section = $(this).parents('section');
                var allItems = $(this).find('.owl-item');
                var activeItems = $(this).find('.owl-item.active');

                allItems.removeClass('enabled');

                if (section.hasClass('testimonials')) {
                    activeItems.eq(1).addClass('enabled');
                }
                else {
                    activeItems.eq(0).addClass('enabled');
                }
            });

            this.element.trigger('translated.owl.carousel');
        }
    };

    var roleAndPlatform = {
        init: function () {
            this.build();
            this.bindEvents();
        },

        build: function () {
            this.btnToTop = $('section.role-and-platform-content .initial-msg i.up');
            this.items = $('section.role-and-platform .item.right');
            this.initialMsg = $('section.role-and-platform-content .initial-msg');
            this.carousel = $('section.role-and-platform-content .carousel');
        },

        bindEvents: function () {
            this.items.bind('click', this.showCarousel);
            this.btnToTop.bind('click', this.goToItems);
        },

        showCarousel: function () {
            roleAndPlatform.items.removeClass('active');

            $(this).addClass('active');

            roleAndPlatform.initialMsg.addClass('hide');
            roleAndPlatform.carousel.removeClass('hide');

            roleAndPlatform.goToCarousel();
        },

        goToItems: function () {
            var posY = $('section.role-and-platform').offset().top;

            $('html, body').animate({scrollTop: posY}, 'fast');
        },

        goToCarousel: function () {
            var posY = $('section.role-and-platform-content .title').offset().top;

            $('html, body').animate({scrollTop: (posY - 50)}, 'fast');
        }
    };

    app.init();
});