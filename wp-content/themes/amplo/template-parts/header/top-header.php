<div class="top-header">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 logo">
                <a href="<?php echo get_home_url() ?>">
                    <img src="<?php echo get_theme_file_uri('assets/img/logo.png') ?>" alt="AMPLO">
                </a>
            </div>
            <div class="col-xs-8 menu">
                <a href="#!" class="menu-mobile"><i class="glyphicon glyphicon-menu-hamburger"></i></a>
                <ul>
                    <li><a href="<?php echo get_site_url() ?>/platform"><span>Platform</span></a></li>
                    <li><a href="<?php echo get_site_url() ?>/institutional"><span>AMPLO is for</span></a></li>
                    <li><a href="<?php echo get_site_url() ?>/schedule-a-demo"><span>Shedule ao demo</span></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>